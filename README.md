## About
This [LaTeX](https://www.latex-project.org) project produces an [essay about essay writing](https://gitlab.com/ilippert/standard-essay/-/blob/main/standard.pdf). I primarily optimised this document for postgraduate students at Master and PhD level. However, undergraduate students are welcome to use it as well.

In the [document](https://gitlab.com/ilippert/standard-essay/-/blob/main/standard.pdf), I differentiate the author's voice (consider the student who authors an essay) from other academics' voices. I discuss how others' voices, i.e. scholarly literature, can be searched, found and reviewed, how it can be referenced and cited. I detail how students can relate to other authors and their voices and texts, thus exploring what it means to quote, paraphrase, summarise. This leads into ways of thinking about how the student's voice can be strengthened.


## History

It emerged 2014 at [IT University of Copenhagen's Technologies in Practice research group](https://tip.itu.dk/), was continued to be developed at [Brandenburg University of Technology's Chair of Technoscience Studies](https://www.b-tu.de/en/fg-technikwissenschaft), and has now found a home in my job at [Goethe University Frankfurt's Institute of Cultural Anthropology and European Ethnology](https://www.goethe-university-frankfurt.de/108852533/Institute_of_Cultural_Anthropology_and_European_Ethnology?locale=en).


## Feedback
Feedback, via [issues](https://gitlab.com/ilippert/standard-essay/-/issues), is welcome. This repo does not welcome advertisement for external services.
